package com.example.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    private CardView btnBuku;
    private CardView btnStudents;
    private CardView btnDataBuku;
    private CardView btnDataStudents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnBuku = findViewById(R.id.btnBuku);
        btnDataBuku = findViewById(R.id.btnDataBuku);
        btnStudents = findViewById(R.id.btnStudents);
        btnDataStudents = findViewById(R.id.btnDataStudents);

        btnBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, BookActivity.class);
                startActivity(i);
            }
        });

        btnDataBuku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, DataBukuActivity.class);
                startActivity(i);
            }
        });

        btnStudents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, StudentsActivity.class);
                startActivity(i);
            }
        });

        btnDataStudents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, DataStudentsActivity.class);
                startActivity(i);
            }
        });

    }
}
