package com.example.perpustakaan;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "perpustakaan.db";

    private static final int DATABSE_VERSION = 4;

    //m_users
    private static final String SQL_CREATE_M_USERS = "CREATE TABLE "+ Table.Users.TABLE_NAME+ " (" +
            " "+ Table.Users._ID +" INTEGER PRIMARY KEY, " +
            " "+ Table.Users.NAMA + " TEXT," +
            " "+ Table.Users.PASSWORD +" TEXT ) ";
    private static final String SQL_DELETE_M_USERS = "DROP TABLE IF EXISTS "+
            Table.Users.TABLE_NAME;

    //m_books
    private static final String SQL_CREATE_M_BOOKS = "CREATE TABLE "+ Table.Books.TABLE_NAME+ " (" +
            " "+ Table.Books._ID +" INTEGER PRIMARY KEY, " +
            " "+ Table.Books.NAMA_BUKU + " TEXT," +
            " "+ Table.Books.TAHUN_TERBIT + " TEXT," +
            " "+ Table.Books.LAMA_PINJAM +" TEXT ) ";
    private static final String SQL_DELETE_M_BOOKS = "DROP TABLE IF EXISTS "+
            Table.Books.TABLE_NAME;

    //m_students
    private static final String SQL_CREATE_M_MAHASIWA = "CREATE TABLE "+ Table.Students.TABLE_NAME+ " (" +
            " "+ Table.Students._ID +" INTEGER PRIMARY KEY, " +
            " "+ Table.Students.NAMA + " TEXT," +
            " "+ Table.Students.CLASS +" TEXT ) ";
    private static final String SQL_DELETE_M_MAHASIWA = "DROP TABLE IF EXISTS "+
            Table.Students.TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABSE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_M_USERS);
        db.execSQL(SQL_CREATE_M_BOOKS);
        db.execSQL(SQL_CREATE_M_MAHASIWA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_M_USERS);
        db.execSQL(SQL_DELETE_M_BOOKS);
        db.execSQL(SQL_DELETE_M_MAHASIWA);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onDowngrade(db, oldVersion, newVersion);
    }


}
