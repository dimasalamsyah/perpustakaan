package com.example.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

public class DataStudentsActivity extends AppCompatActivity {

    //buat list
    private RecyclerView recyclerView;
    private StudentsAdapter adapter;

    private DBHelper dbHelper;
    private ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_students);

        dbHelper = new DBHelper(this);

        recyclerView = findViewById(R.id.recyclerView);

        loadData();
    }

    private void loadData(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Table.Students.NAMA,
                Table.Students.CLASS
        };

        Cursor cursor = db.query(
                Table.Students.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        ArrayList<Students> list = new ArrayList<Students>();

        //while
        while (cursor.moveToNext()){

            String nama = cursor.getString(
                    cursor.getColumnIndexOrThrow(Table.Students.NAMA)
            );
            String kelas = cursor.getString(
                    cursor.getColumnIndexOrThrow(Table.Students.CLASS)
            );

            Students students = new Students();
            students.nama = nama;
            students.kelas = kelas;

            list.add(students);

        } //akhir while

        //inisiasi recylerview
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new StudentsAdapter(list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DataStudentsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //fungsi untuk memunculkan list

    }
}
