package com.example.perpustakaan;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * Created by Dimas Alamsyah on 12/14/2019
 */
public class BukuAdapter extends RecyclerView.Adapter<BukuAdapter.MyViewHolder> {

    ArrayList<Book> list = new ArrayList<Book>();

    //custrucntor
    public BukuAdapter(ArrayList<Book> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public BukuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_book, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BukuAdapter.MyViewHolder holder, int i) {
        Book book = list.get(i);
        holder.tvBook.setText( book.namaBuku );
        holder.tvTahunTerbit.setText( book.tahunTerbit );
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvBook;
        public TextView tvTahunTerbit;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvBook = itemView.findViewById(R.id.tvBuku);
            tvTahunTerbit = itemView.findViewById(R.id.tvTahunTerbit);
        }
    }
}
