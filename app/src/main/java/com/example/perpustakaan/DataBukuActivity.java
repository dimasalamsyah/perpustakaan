package com.example.perpustakaan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class DataBukuActivity extends AppCompatActivity {

    //buat list
    private RecyclerView recyclerView;
    private BukuAdapter adapter;

    private ArrayList<String> list = new ArrayList<String>();
    private DBHelper dbHelper;
    private ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_buku);

        dbHelper = new DBHelper(this);
        recyclerView = findViewById(R.id.recyclerView);
        btnBack = findViewById(R.id.imgBack);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loadData();

    }

    private void loadData(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                Table.Books.NAMA_BUKU,
                Table.Books.LAMA_PINJAM,
                Table.Books.TAHUN_TERBIT
        };

        Cursor cursor = db.query(
                Table.Books.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        ArrayList<Book> list = new ArrayList<Book>();

        //while
        while (cursor.moveToNext()){

            String namaBuku = cursor.getString(
                    cursor.getColumnIndexOrThrow(Table.Books.NAMA_BUKU)
            );
            String lamaPinjam = cursor.getString(
                    cursor.getColumnIndexOrThrow(Table.Books.LAMA_PINJAM)
            );

            String tahunTerbit = cursor.getString(
                    cursor.getColumnIndexOrThrow(Table.Books.TAHUN_TERBIT)
            );

            Book book = new Book();
            book.namaBuku = namaBuku;
            book.lamaPinjam = lamaPinjam;
            book.tahunTerbit = tahunTerbit;

            list.add(book);

        } //akhir while

        //inisiasi recylerview
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new BukuAdapter(list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DataBukuActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //fungsi untuk memunculkan list

    }
}
