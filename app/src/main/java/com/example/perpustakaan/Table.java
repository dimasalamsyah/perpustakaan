package com.example.perpustakaan;

import android.provider.BaseColumns;

public class Table {

    public Table() {
    }

    /* Inner class that defines the table contents */
    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "m_users";
        public static final String NAMA = "nama";
        public static final String PASSWORD = "password";
    }

    /* Inner class that defines the table contents */
    public static class Books implements BaseColumns {
        public static final String TABLE_NAME = "m_books";
        public static final String NAMA_BUKU = "nama_buku";
        public static final String LAMA_PINJAM = "lama_pinjam";
        public static final String TAHUN_TERBIT = "tahun_terbit";
    }

    /* Inner class that defines the table contents */
    public static class Students implements BaseColumns {
        public static final String TABLE_NAME = "m_students";
        public static final String NAMA = "nama";
        public static final String CLASS = "kelas";
    }

}
