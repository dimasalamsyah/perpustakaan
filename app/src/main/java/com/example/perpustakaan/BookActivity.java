package com.example.perpustakaan;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class BookActivity extends AppCompatActivity {

    private Button btnSimpan;
    private ImageView btnBack;
    private EditText etNamaBuku;
    private EditText etLamaPinjam;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        dbHelper = new DBHelper(this);

        btnBack = findViewById(R.id.imgBack);
        btnSimpan = findViewById(R.id.btnSimpan);
        etNamaBuku = findViewById(R.id.etNamaBuku);
        etLamaPinjam = findViewById(R.id.etLamaPinjam);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //mode
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                //isi nilai
                ContentValues values = new ContentValues();
                values.put(Table.Books.NAMA_BUKU, etNamaBuku.getText().toString());
                values.put(Table.Books.LAMA_PINJAM, etLamaPinjam.getText().toString());

                //simpan data
                long newRowId = db.insert(Table.Books.TABLE_NAME, null, values);

                //jika data berhasil tersimpan newRowId akan 1
                if(newRowId > 0){

                    //untuk menutup activity
                    finish();
                    //dialogError();
                }else{
                    dialogError();
                }

            }
        });

    }

    private void dialogError(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        // set title dialog
        alertDialogBuilder.setTitle("Pesan");
        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Simpan data tidak berhasil!")
                .setIcon(R.drawable.libary_logo)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        finish();
                    }
                });
        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();
        // menampilkan alert dialog
        alertDialog.show();
    }
}
